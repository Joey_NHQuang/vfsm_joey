using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hmx.vodafone.web.Models.UserViewModels
{
    public class DeleteUserDeviceViewModel
    {
        [Column("DEVICE_ID")]
        [Display(Name = "Device ID")]
        public string DeviceId { get; set; }


        [Column("USER_ID")]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Display(Name = "Page")]
        public int Page { get; set; }
    }
}