using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace hmx.vodafone.web.Models.UserViewModels
{
    public class UserNickNameModifyViewModel
    {
        [Required]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Required]
        [Display(Name = "Imei")]
        public string DeviceUid { get; set; }

        [Display(Name = "Input New Nick Name")]
        public string NickName { get; set; }
    }
}