using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace hmx.vodafone.web.Models.UserViewModels
{
    public class DeviceRegisterViewModel
    {
        [Required]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Display(Name = "Input Device Uid")]
        public string DeviceUid { get; set; }

        [Display(Name = "Input Device Name")]
        public string DeviceName { get; set; }
    }
}