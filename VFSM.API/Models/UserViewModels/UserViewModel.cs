using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hmx.vodafone.web.Models.UserViewModels
{
    public class UserViewModel
    {
        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Column("USER_ID")]
        [Display(Name = "Customer ID")]
        public string UserId { get; set; }

        [Column("NICK_NAME")]
        [Display(Name = "Name")]
        public string NickName { get; set; }

        [Column("MOBILE_TYPE")]
        [Display(Name = "Mobile Type")]
        public string MobileType { get; set; }

        [Column("IMEI")]
        [Display(Name = "Imei")]
        public string Imei { get; set; }

        [Column("DEVICE_COUNT")]
        [Display(Name = "Device Count")]
        public int DeviceCount { get; set; }
    }
}