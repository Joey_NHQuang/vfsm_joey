using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace hmx.vodafone.web.Models.UserViewModels
{
    public class SendMessageViewModel
    {
        [Required]
        [Display(Name = "Imei")]
        public string DeviceUid { get; set; }

        [Display(Name = "Input Latitude")]
        public double Lat { get; set; }

        [Display(Name = "Input Longitude")]
        public double Lng { get; set; }

        [Display(Name = "Battey Level")]
        [Range(0, 100)]
        public int Battery { get; set; }

        [Display(Name = "Message Type")]
        public string MessageType { get; set; }

        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Display(Name = "Page")]
        public int Page { get; set; }
    }
}