using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace hmx.vodafone.web.Models.AdminUserViewModels
{
    public class AdminUserViewModel
    {
        [Display(Name = "Id")]
        public string Id { get; set; }
        [Display(Name = "User name")]
        public string UserName { get; set; } 
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Phone confirmed")]
        public bool PhoneNumberConfirmed { get; set; }
        [Display(Name = "Email confirmed")]
        public bool EmailConfirmed { get; set; }
        [Display(Name = "Roles")]
        public IList<string> Roles { get; set; }
    }
}