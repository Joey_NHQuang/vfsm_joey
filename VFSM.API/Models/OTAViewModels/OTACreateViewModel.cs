using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace hmx.vodafone.web.Models.OTAViewModels
{
    public class OTACreateViewModel
    {
        [Required]
        [Display(Name = "Model ID")]
        public string ModelId { get; set; }

        [Required]
        [Display(Name = "Version")]
        [RegularExpression(@"^\d+\.\d+\.\d+\.\d+")]
        public string Version { get; set; }

        [Required]
        [Display(Name = "Category")]
        public string Category { get; set; }
        
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        
        [Required]
        [Display(Name = "File")]
        public IFormFile File { get; set; }
    }
}