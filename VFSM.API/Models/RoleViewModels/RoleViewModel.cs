using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace hmx.vodafone.web.Models.RoleViewModels
{
    public class RoleViewModel
    {
        [Display(Name = "Role ID")]
        public string Id { get; set; }

        [Required]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; }

        [Display(Name = "User Count")]
        public int UserCount { get; set; }
    }
}
