﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using hmx.vodafone.web.Constants;

namespace hmx.vodafone.web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) => {
                    // 실행환경에 따른 appsettings.json 파일 선택
                    IHostingEnvironment env = hostingContext.HostingEnvironment;
                    if (env.EnvironmentName == VF_CONSTANT.Release) {
                        config.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
                    }
                    else
                    {
                        config.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    }
                })
                .UseKestrel()
                .UseStartup<Startup>()
                .Build();
    }
}
