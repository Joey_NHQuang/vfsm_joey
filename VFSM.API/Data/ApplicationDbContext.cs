﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;

namespace hmx.vodafone.web.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string, IdentityUserClaim<string>,
    ApplicationUserRole, IdentityUserLogin<string>,
    IdentityRoleClaim<string>, IdentityUserToken<string>>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<ApplicationUser>().ToTable("T_ADMIN_USERS");
            builder.Entity<ApplicationRole>().ToTable("T_ADMIN_ROLES");
            builder.Entity<ApplicationUserRole>().ToTable("T_ADMIN_USER_ROLES");
            builder.Entity<IdentityUserClaim<string>>().ToTable("T_ADMIN_USER_CLAIMS");
            builder.Entity<IdentityUserLogin<string>>().ToTable("T_ADMIN_USER_LOGINS");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("T_ADMIN_ROLE_CLAIMS");
            builder.Entity<IdentityUserToken<string>>().ToTable("T_ADMIN_USER_TOKENS");

            builder.Entity<ApplicationUserRole>(userRole =>
            {
                userRole.HasKey(ur => new { ur.UserId, ur.RoleId });

                userRole.HasOne(ur => ur.Role)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.RoleId)
                    .IsRequired();

                userRole.HasOne(ur => ur.User)
                    .WithMany(r => r.UserRoles)
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });
        }
    }
}
