using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Constants;
using ReflectionIT.Mvc.Paging;

namespace hmx.vodafone.web.Controllers
{
    [Authorize(Roles = "Admin, Service Managements")]
    public class ModelController : BaseController
    {
        public ModelController(VodafoneContext context, 
                            UserManager<ApplicationUser> userManager)
        :base(context, userManager)
        {
        }

        #region Index
        // GET: VodafoneModel
        [HttpGet]
        public async Task<IActionResult> Index(string filter, string sortExpression = "ModelId", int page = 1)
        {
            var qry = _context.VodafoneModels.AsNoTracking();
            if (!string.IsNullOrEmpty(filter))
            {
                qry = qry.Where(m => m.ModelId.Contains(filter));
            }
            var model = await PagingList<VodafoneModel>.CreateAsync(qry, VF_CONSTANT.PageSize, page, sortExpression, "ModelId");
            model.RouteValue = new RouteValueDictionary { {"filter", filter} };
            return View(model);
        }
        #endregion

        #region Details
        // GET: VodafoneModel/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var vodafoneModel = await _context.VodafoneModels
                .SingleOrDefaultAsync(m => m.ModelId == id);
            if (vodafoneModel == null)
            {
                return NotFound();
            }

            return View(vodafoneModel);
        }
        #endregion

        #region Create
        // GET: VodafoneModel/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: VodafoneModel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ModelId,Name,PrivacyId,TermsId,Description,DelFlag")] VodafoneModel vodafoneModel)
        {
            if (ModelState.IsValid)
            {
                vodafoneModel.CreateDate = DateTime.UtcNow;
                vodafoneModel.DelFlag = false;
                _context.Add(vodafoneModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(vodafoneModel);
        }
        #endregion

        #region Edit
        // GET: VodafoneModel/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var vodafoneModel = await _context.VodafoneModels.SingleOrDefaultAsync(m => m.ModelId == id);
            if (vodafoneModel == null)
            {
                return NotFound();
            }
            return View(vodafoneModel);
        }

        // POST: VodafoneModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ModelId,Name,CreateDate,PrivacyId,TermsId,Description,DelFlag")] VodafoneModel vodafoneModel)
        {
            if (id != vodafoneModel.ModelId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    vodafoneModel.ModifiedDate = DateTime.UtcNow;
                    _context.Update(vodafoneModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await VodafoneModelExists(vodafoneModel.ModelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(vodafoneModel);
        }
        #endregion

        #region Delete
        // GET: VodafoneModel/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var vodafoneModel = await _context.VodafoneModels
                .SingleOrDefaultAsync(m => m.ModelId == id);
            if (vodafoneModel == null)
            {
                return NotFound();
            }

            return View(vodafoneModel);
        }

        // POST: VodafoneModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var vodafoneModel = await _context.VodafoneModels.SingleOrDefaultAsync(m => m.ModelId == id);
            _context.VodafoneModels.Remove(vodafoneModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        #endregion
    }
}
