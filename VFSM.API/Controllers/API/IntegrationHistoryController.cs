using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Constants;
using ReflectionIT.Mvc.Paging;
using VinaHR.DAL;

namespace hmx.vodafone.web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class IntegrationHistoryController : BaseController
    {
        public IntegrationHistoryController(VodafoneContext context, UserManager<ApplicationUser> userManager)
        :base(context, userManager)
        {}

        #region Index
        [HttpGet]
        public async Task<IActionResult> Index(string filter, string sortExpression = "-ReqDate", int page = 1, int pageSize = 20)
        {
            var qry = _context.VodafoneAccountIntegrationHistories.AsNoTracking();
            if (!string.IsNullOrEmpty(filter))
            {
                qry = qry.Where(m => m.Imei.Contains(filter));
            }

            var list = PagingList.GetPage<VodafoneAccountIntegrationHistory>(qry, page, pageSize);                        
            return Ok(list);
        }
        #endregion
    }
}