using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Models.OTAViewModels;
using hmx.vodafone.web.Config;
using hmx.vodafone.web.Constants;
using VFSM.Services;
using Amazon.S3;
using Amazon.S3.Transfer;
using Amazon.S3.Model;
using ReflectionIT.Mvc.Paging;
using VFSM.Services.Extensions;

namespace hmx.vodafone.web.Controllers
{
    [Authorize(Roles = "Admin, Service Managements")]
    public class OTAController : BaseController
    {
        private readonly AWSConfig _awsConfig;
        private TransferUtility FileTransferUtility;

        private IAmazonS3 _s3Client;

        public OTAController(VodafoneContext context, UserManager<ApplicationUser> userManager, IAmazonS3 s3Client, IOptions<AWSConfig> awsConfig)
        :base (context, userManager)
        {
            _s3Client = s3Client;
            FileTransferUtility = new TransferUtility(s3Client);
            _awsConfig = awsConfig.Value;
        }
        
        #region Index
        public async Task<IActionResult> Index(string modelId, string category, string filter, string sortExpression = "-CreatedDate", int page = 1)
        {
            // Dropdown list of models
            ViewData["ModelList"] = await GetDropdownListOfModelsWithAllModel(modelId);
            ViewData["CategoryList"] = GetDropdownListOfModelOTACategoryWithAllCategory(category);
            ViewData["ModelId"] = modelId;
            ViewData["Category"] = category;

            var qry = _context.VodafoneOtas.AsNoTracking();
            
            if (!String.IsNullOrEmpty(modelId))
            {
                qry = qry.Where(o => o.ModelId.Contains(modelId));
            }
            if (!String.IsNullOrEmpty(category))
            {
                qry = qry.Where(o => o.Category.Contains(category));
            }
            if (!String.IsNullOrEmpty(filter)) 
            {
                qry = qry.Where(o => o.Version.Contains(filter));
            }
            
            var model = await PagingList<VodafoneModelOTA>.CreateAsync(qry, VF_CONSTANT.PageSize, page, sortExpression, "-CreateDate");
            model.RouteValue = new RouteValueDictionary { {"filter", filter} };
            return View(model);
        }
        #endregion

        #region Detail
        public async Task<IActionResult> Details(string modelId, string category, string version)
        {
            if (string.IsNullOrEmpty(modelId) || string.IsNullOrEmpty(category) || string.IsNullOrEmpty(version))
            {
                return NotFound();
            }
            var ota = await _context.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == modelId && o.Category == category && o.Version == version);
            if (ota == null)
            {
                return NotFound();
            }
            return View(ota);
        }
        #endregion
        
        #region Create
        public async Task<IActionResult> Create()
        {
            ViewData["ModelList"] = await GetDropdownListOfModelsWithoutAllModel(String.Empty);
            ViewData["CategoryList"] = GetDropdownListOfModelOTACategory(string.Empty);
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(OTACreateViewModel ota)
        {
            // TODO: file model validation check
            if(!await VodafoneModelExists(ota.ModelId))
            {
                ModelState.AddModelError("Model ID", "Model not found. Check your Model ID.");
            }
            if (await VodafoneOTAExists(ota.ModelId, ota.Category, ota.Version))
            {
                ModelState.AddModelError("Version", "Duplicate version.");
            }

            if (ModelState.IsValid)
            {
                // TODO: AWS file upload
                await FileTransferUtility.UploadAsync(
                            ota.File.OpenReadStream(), 
                            _awsConfig.AWS_BUCKET, 
                            AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName));
                
                GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                request.BucketName = _awsConfig.AWS_BUCKET;
                request.Key        = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName);
                request.Expires    = DateTime.Now.AddHours(1);
                request.Protocol   = Protocol.HTTPS;
                
                var url = new Uri(FileTransferUtility.S3Client.GetPreSignedURL(request));
                
                // TODO: Save uploaded info to T_MODEL_OTA table
                var otaModel = new VodafoneModelOTA{
                    ModelId = ota.ModelId,
                    Version = ota.Version,
                    Category = ota.Category,
                    IsActive = ota.IsActive,
                    OtaUrl = url.Scheme + "://" + url.Host + url.LocalPath,
                    FileName = AWSS3PathExtensions.GenerateFilenameWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName),
                    FileSize = ota.File.Length,
                    CreatedDate = DateTime.UtcNow
                };
                _context.Add(otaModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ota);
        }
        #endregion
        
        #region Edit
        public async Task<IActionResult> Edit(string modelId, string version, string category)
        {
            if (string.IsNullOrEmpty(modelId) || string.IsNullOrEmpty(version) || string.IsNullOrEmpty(category))
            {
                return NotFound();
            }

            var ota = await _context.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == modelId && o.Version == version && o.Category == category);
            if (ota == null)
            {
                return NotFound();
            }

            var otaUpdateModel = new OTAUpdateViewModel{
                ModelId = ota.ModelId,
                Version = ota.Version,
                Category = ota.Category,
                IsActive = ota.IsActive,
                CreatedDate = ota.CreatedDate,
                ModifiedDate = ota.ModifiedDate
            };

            return View(otaUpdateModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string modelId, string version, string category, OTAUpdateViewModel ota)
        {
            if (modelId != ota.ModelId || version != ota.Version || category != ota.Category)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var originalOta = await _context.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == ota.ModelId && o.Version == ota.Version && o.Category == ota.Category);
                    if (ota.File != null)
                    {
                        if (!string.IsNullOrEmpty(originalOta.OtaUrl))
                        {
                            var uri = new Uri(originalOta.OtaUrl);
                            string[] splitUri = uri.LocalPath.Split("/");
                            DeleteObjectRequest deleteObjectRequest = null;
                            if (splitUri.Length == 4){
                                
                                deleteObjectRequest =  new DeleteObjectRequest
                                        {
                                            BucketName = _awsConfig.AWS_BUCKET,
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithoutCategory(originalOta.ModelId, originalOta.Version, originalOta.FileName)
                                        };
                            } else {
                                deleteObjectRequest =
                                    new DeleteObjectRequest
                                        {
                                            BucketName = _awsConfig.AWS_BUCKET,
                                            Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithCategory(originalOta.ModelId, originalOta.Version, originalOta.Category, originalOta.FileName)
                                        };
                            }
                            await FileTransferUtility.S3Client.DeleteObjectAsync(deleteObjectRequest);
                        }

                        await FileTransferUtility.UploadAsync(
                            ota.File.OpenReadStream(), 
                            _awsConfig.AWS_BUCKET, 
                            AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName));
                
                        GetPreSignedUrlRequest request = new GetPreSignedUrlRequest();
                        request.BucketName = _awsConfig.AWS_BUCKET;
                        request.Key        = AWSS3PathExtensions.GenerateAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName);
                        request.Expires    = DateTime.Now.AddHours(1);
                        request.Protocol   = Protocol.HTTPS;
                        
                        var url = new Uri(FileTransferUtility.S3Client.GetPreSignedURL(request));
                        
                        originalOta.IsActive = ota.IsActive;
                        originalOta.OtaUrl = url.Scheme + "://" + url.Host + url.LocalPath;
                        originalOta.FileName = AWSS3PathExtensions.GenerateFilenameWithCategory(ota.ModelId, ota.Version, ota.Category, ota.File.FileName);
                        originalOta.FileSize = ota.File.Length;
                    }
                    else 
                    {
                        originalOta.IsActive = ota.IsActive;
                    }

                    originalOta.ModifiedDate = DateTime.UtcNow;
                    _context.Update(originalOta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await VodafoneModelExists(ota.ModelId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ota);
        }
        #endregion

        #region Delete
        public async Task<IActionResult> Delete(string modelId, string category, string version)
        {
            if (string.IsNullOrEmpty(modelId) || string.IsNullOrEmpty(version) || string.IsNullOrEmpty(category))
            {
                return NotFound();
            }

            var ota = await _context.VodafoneOtas
                .SingleOrDefaultAsync(o => o.ModelId == modelId && o.Category == category && o.Version == version);
            if (ota == null)
            {
                return NotFound();
            }

            return View(ota);
        }

        // POST: VodafoneModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string modelId, string category, string version)
        {
            var ota = await _context.VodafoneOtas.SingleOrDefaultAsync(o => o.ModelId == modelId && o.Category == category && o.Version == version);
            
            if (!string.IsNullOrEmpty(ota.OtaUrl))
            {
                var uri = new Uri(ota.OtaUrl);
                string[] splitUri = uri.LocalPath.Split("/");
                DeleteObjectRequest deleteObjectRequest = null;
                if (splitUri.Length == 4){
                    
                    deleteObjectRequest = new DeleteObjectRequest
                    {
                        BucketName = _awsConfig.AWS_BUCKET,
                        Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithoutCategory(ota.ModelId, ota.Version, ota.FileName)
                    };
                } else {
                    deleteObjectRequest = new DeleteObjectRequest
                    {
                        BucketName = _awsConfig.AWS_BUCKET,
                        Key = AWSS3PathExtensions.GenerateDeleteAWSS3KeyWithCategory(ota.ModelId, ota.Version, ota.Category, ota.FileName)
                    };
                }
                
                await FileTransferUtility.S3Client.DeleteObjectAsync(deleteObjectRequest);
            }

            _context.VodafoneOtas.Remove(ota);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        #endregion

        
    }
}