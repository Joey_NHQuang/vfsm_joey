using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Constants; 

namespace hmx.vodafone.web.Controllers
{
    [Authorize(Roles = "Admin, CS Read, CS Write")]
    public class BeaconController : BaseController
    {
        public BeaconController(VodafoneContext context, UserManager<ApplicationUser> userManager)
        :base(context, userManager)
        {
        }

        #region Index
        public IActionResult Index(string mac)
        {
            if (string.IsNullOrEmpty(mac))
            {
                return View();
            }
            else
            {
                ViewData["mac"] = mac;
                var beacon = _context.VodafoneBeacons.Where(b => b.Mac == mac).FirstOrDefault();
                return View(beacon);
            }
        }
        #endregion
    }
}