using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using hmx.vodafone.web.Config;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.UserViewModels;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Constants;
using ReflectionIT.Mvc.Paging; 
using Newtonsoft.Json;

namespace hmx.vodafone.web.Controllers
{
    [Authorize(Roles = "Admin, CS Read, CS Write")]
    public class UserController : BaseController
    {
        private HttpClient _httpClient;

        public UserController(VodafoneContext context, UserManager<ApplicationUser> userManager, IOptions<VodafoneConfig> vodafoneConfig)
        : base(context, userManager)
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri(vodafoneConfig.Value.ApiServerUrl);
            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
        }

        #region Index
        [Authorize(Roles = "Admin, CS Read, CS Write")]
        public async Task<IActionResult> Index(string filter, string sortExpression = "-CreatedDate", int page = 1)
        {
            IQueryable<UserViewModel> qry = null;

            if (string.IsNullOrEmpty(filter)) 
            {
                qry = from u in _context.VodafoneUsers
                      select new UserViewModel 
                      { 
                          CreatedDate = u.CreatedDate,
                          UserId = u.UserId,
                          NickName = u.NickName,
                          MobileType = u.MobileType,
                          Imei = _context.VodafeonDevices.Where(d => d.DeviceId == _context.VodafoneUserDevices.Where(x => x.UserId == u.UserId).FirstOrDefault().DeviceId).FirstOrDefault().DeviceUid,
                          DeviceCount = _context.VodafoneUserDevices.Where(x => x.UserId == u.UserId).Count() 
                      };
            } 
            else 
            {
                qry = from d in _context.VodafeonDevices
                      join ud in _context.VodafoneUserDevices on d.DeviceId equals ud.DeviceId
                      join u in _context.VodafoneUsers on ud.UserId equals u.UserId
                      where d.DeviceUid == filter
                      select new UserViewModel 
                      {
                          CreatedDate = u.CreatedDate,
                          UserId = u.UserId,
                          NickName = u.NickName,
                          MobileType = u.MobileType,
                          Imei = filter,
                          DeviceCount = _context.VodafoneUserDevices.Where(x => x.UserId == u.UserId).Count() 
                      };
            }

            var model = await PagingList<UserViewModel>.CreateAsync(qry, VF_CONSTANT.PageSize, page, sortExpression, "-CreateDate");
            model.RouteValue = new RouteValueDictionary { {"filter", filter} };

            ViewData["IsCSRead"] = IsCSReadUser();
            return View(model);
        }
        #endregion
        
        #region DeviceList
        [Authorize(Roles = "Admin, CS Read, CS Write")]
        public async Task<IActionResult> DeviceList(string filter, int page = 1)
        {
            if (string.IsNullOrEmpty(filter))
            {
                return NotFound();
            }

            ViewData["userid"] = filter;
            ViewData["devicepage"] = page;
            ViewData["IsCSRead"] = IsCSReadUser();
            var model = from ud in _context.VodafoneUserDevices.Where(x => x.UserId == filter)
                      join d in _context.VodafeonDevices on ud.DeviceId equals d.DeviceId
                      select new UserDeviceViewModel 
                      {
                          CreatedDate = d.CreatedDate,
                          DeviceId = d.DeviceId,
                          DeviceUid = d.DeviceUid,
                          Name = d.Name,
                          Imei = d.DeviceUid,
                          Role = ud.Role,
                          Status = d.Status.Replace("\"", "").Replace("[", "").Replace("]", ""),
                          UserId = filter
                      };
            
            
            return PartialView("DeviceListPartial", await PaginatedList<UserDeviceViewModel>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize));
        }
        #endregion
        
        #region ActivityList
        [Authorize(Roles = "Admin, CS Read, CS Write")]
        public async Task<IActionResult> ActivityList(string id, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            ViewData["deviceid"] = id;
            ViewData["activitypage"] = page;

            var model = _context.VodafoneActivities.Where(x => x.DeviceId == id).OrderByDescending(x=>x.CreatedDate);
            return PartialView("ActivityListPartial", await PaginatedList<VodafoneActivity>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize));
        }
        #endregion

        #region ActivitySeriesList
        [Authorize(Roles = "Admin, CS Read, CS Write")]
        public async Task<IActionResult> ActivitySeriesList(string id, int page = 1)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            ViewData["activityid"] = id;
            ViewData["activitySeriespage"] = page;

            var model = _context.VodafoneActivitySeries.Where(x => x.ActId == id).OrderByDescending(x=>x.CreatedDate);
            return PartialView("ActivitySeriesListPartial", await PaginatedList<VodafoneActivitySeries>.CreateAsync(model.AsNoTracking(), page, VF_CONSTANT.PageSize));
        }
        #endregion

        #region Register
        [HttpGet]
        [Authorize(Roles = "Admin, CS Write")]
        public IActionResult Register(string userId)
        {
            var model = new DeviceRegisterViewModel { UserId = userId };
            return PartialView("RegisterPopup", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, CS Write")]
        public async Task<IActionResult> Register(DeviceRegisterViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                var user = await _context.VodafoneUsers.Where(u => u.UserId == model.UserId).FirstOrDefaultAsync();
                var device = await _context.VodafeonDevices.Where(d => d.DeviceUid == model.DeviceUid).FirstOrDefaultAsync();
                
                if (device == null)
                {
                    var generatedDeviceId = System.Guid.NewGuid().ToString();
                    var savedDevice = await _context.VodafeonDevices.AddAsync(new VodafoneDevice{
                        DeviceVersion = "0.0.0.0",
                        DeviceId = generatedDeviceId,
                        ImgUrl = string.Empty,
                        DeviceUid = model.DeviceUid,
                        Name = model.DeviceName,
                        ActivatedDate = DateTime.UtcNow,
                        Activated = true,
                        ModelId = "31-00006",
                        Status = "[\"OK\"]",
                        StatusLevel = "NORMAL",
                        CreatedDate = DateTime.UtcNow
                    });
                    await _context.SaveChangesAsync();
                    
                    await _context.VodafoneUserDevices.AddAsync(new VodafoneUserDevice{
                        UserId = model.UserId,
                        DeviceId = savedDevice.Entity.DeviceId,
                        RegName = savedDevice.Entity.Name,
                        Role = "ROOT",
                        CreatedDate = DateTime.UtcNow
                    });
                    await _context.SaveChangesAsync();
                    
                }
                else
                {
                    var userDevice = _context.VodafoneUserDevices.Where(ud => ud.DeviceId == device.DeviceId).FirstOrDefault();
                    if (userDevice != null)
                    {
                        // 등록유저가 있을 때 - UserDevice에 SUB로 등록
                        await _context.VodafoneUserDevices.AddAsync(new VodafoneUserDevice{
                            UserId = model.UserId,
                            DeviceId = device.DeviceId,
                            RegName = device.Name,
                            Role = "SUB",
                            CreatedDate = DateTime.UtcNow
                        });
                    }
                    else
                    {
                        // 등록유저가 없을 때 - UserDeivce에 ROOT로 등록
                        await _context.VodafoneUserDevices.AddAsync(new VodafoneUserDevice{
                            UserId = model.UserId,
                            DeviceId = device.DeviceId,
                            RegName = device.Name,
                            Role = "ROOT",
                            CreatedDate = DateTime.UtcNow
                        });
                    }
                    await _context.SaveChangesAsync();
                }
                result.Message = "Registration succeeded.";
                result.RedirectMethod = "refresh";
            }
            else
            {
                result.Message = "Paramater is not vaild.";
            }
            return Json(result);
        }
        #endregion
    
        #region UserNickNameModify
        [HttpGet]
        [Authorize(Roles = "Admin, CS Write")]
        public IActionResult UserNickNameModify(string userId, string deviceUid)
        {
            var model = new UserNickNameModifyViewModel { 
                UserId = userId,
                DeviceUid = deviceUid
                };
            return PartialView("UserNickNameModifyPopup", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, CS Write")]
        public async Task<IActionResult> UserNickNameModify(UserNickNameModifyViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                var user = await _context.VodafoneUsers.Where(u => u.UserId == model.UserId).FirstOrDefaultAsync();
                if (user == null)
                {
                    result.Message = "User not found. check your USER ID.";
                }
                else
                {
                    user.NickName = model.NickName;
                    user.ModifiedDate = DateTime.UtcNow;
                    _context.VodafoneUsers.Update(user);
                    await _context.SaveChangesAsync();
                    result.Success = true;
                    result.Message = "The user's nick name has been changed.";
                    result.RedirectMethod = "refresh";
                }
            }
            else
            {
                result.Message = "Paramater is not vaild.";
            }
            return Json(result);
        }
        #endregion
    
        #region DeviceNameModify
        [HttpGet]
        [Authorize(Roles = "Admin, CS Write")]
        public IActionResult DeviceNameModify(string deviceId, string userId, int page)
        {
            var device = _context.VodafeonDevices.Where(ud => ud.DeviceId == deviceId).FirstOrDefault();
            var model = new DeviceNameModifyViewModel { 
                    DeviceId = deviceId,
                    DeviceUid = device.DeviceUid,
                    UserId = userId,
                    Page = page
                };
            return PartialView("DeviceNameModifyPopup", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, CS Write")]
        public async Task<IActionResult> DeviceNameModify(DeviceNameModifyViewModel model, int page)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                var device = await _context.VodafeonDevices.Where(d => d.DeviceId == model.DeviceId).FirstOrDefaultAsync();
                if (device == null)
                {
                    result.Message = "Deivce is not found. Check your DEVICE ID.";
                }
                else
                {
                    device.Name = model.Name;
                    device.ModifiedDate = DateTime.UtcNow;
                    _context.VodafeonDevices.Update(device);
                    await _context.SaveChangesAsync();
                    result.Success = true;
                    result.Message = "Deivce Name Modify is success.";
                    result.RedirectMethod = "deviceList";
                    result.ParameterId = model.UserId;
                    result.ParameterPage = page;
                }
            }
            else
            {
                result.Message = "Paramater is not vaild.";
            }
            return Json(result);
        }
        #endregion
    
        #region SendMessage
        [HttpGet]
        [Authorize(Roles = "Admin, CS Write")]
        public IActionResult SendMessage(string deviceUid, string userId, int page)
        {
            var model = new SendMessageViewModel {
                DeviceUid = deviceUid,
                UserId = userId,
                Page = page
            };
            ViewData["EventTypeList"] = GetDropdownListOfEventType();
            return PartialView("SendMessagePopup", model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, CS Write")]
        public async Task<IActionResult> SendMessage(SendMessageViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                using (HttpResponseMessage response = await _httpClient.GetAsync(
                    string.Format("message?deviceId={0}&lat={1}&lng={2}&battery={3}&msgType={4}", model.DeviceUid, model.Lat, model.Lng, model.Battery,model.MessageType)))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        var responseResult = JsonConvert.DeserializeObject<ResponseModel>(await response.Content.ReadAsStringAsync());
                        if (responseResult.Success)
                        {
                            result.Success = true;
                            result.Message = "Message sent complete.";
                            result.RedirectMethod = "deviceList";
                            result.ParameterId = model.UserId;
                            result.ParameterPage = model.Page;
                        }
                    }
                }
            }
            return Json(result);
        }
        #endregion
    
        #region DeleteUserDevice
        [HttpGet]
        [Authorize(Roles = "Admin, CS Write")]
        public IActionResult DeleteUserDevice(string deviceId, string userId, int page)
        {
            var model = new DeleteUserDeviceViewModel{
                UserId = userId,
                DeviceId = deviceId,
                Page = page
            };
            return PartialView("DeleteUserDevicePopup", model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin, CS Write")]
        public async Task<IActionResult> DeleteUserDevice(DeleteUserDeviceViewModel model)
        {
            var result = new ResponseModel { Success = false };
            if (ModelState.IsValid)
            {
                var userDevice = await _context.VodafoneUserDevices.Where(ud => ud.UserId == model.UserId && ud.DeviceId == model.DeviceId).FirstOrDefaultAsync();
                if (userDevice == null)
                {
                    result.Message = "UserDevice not found. Check your USER ID OR DEVICE ID.";
                }
                else
                {
                    _context.VodafoneUserDevices.Remove(userDevice);
                    await _context.SaveChangesAsync();
                    result.Success = true;
                    result.Message = "Delete success.";
                    result.RedirectMethod = "refresh";
                }
            }
            else
            {
                result.Message = "Paramater is not vaild.";
            }
            return Json(result);
        }
        #endregion
    }
}