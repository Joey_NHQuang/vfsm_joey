using System;
using System.Configuration;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Constants;

namespace hmx.vodafone.web.Controllers
{
    public class BaseController : Controller
    {
        protected VodafoneContext _context { get; set; }
        protected UserManager<ApplicationUser> _userManager;

        protected string BuildType
        {
            get { return ConfigurationManager.AppSettings["BuildType"]; }
        }

        protected bool IsAdminUser()
        {
            return User.IsInRole("Admin");
        }

        protected bool IsCSReadUser()
        {
            return User.IsInRole("CS Read");
        }

        public BaseController(VodafoneContext context,
            UserManager<ApplicationUser> userManager) 
        {
            _context = context;
            _userManager = userManager;
        }

        #region Exception helpers
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, 
                                                            ActionExecutionDelegate next)
        {
            ViewBag.MenuItems = MenuItemListModel.GetEmptyMenus();
            ViewBag.BuildType = BuildType;
            if (User.Identity.IsAuthenticated)
            {
                var clam = User.Claims;
                var user = await _userManager.FindByNameAsync(User.Identity.Name);
                ViewBag.MenuItems = MenuItemListModel.GetMenus(await _userManager.GetRolesAsync(user));
            }

            await next();
        }
        
        public void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        public void AddErrors(Exception e)
        {
            if (e is AggregateException)
            {
                foreach (var ae in ((AggregateException)e).Flatten().InnerExceptions)
                {
                    ModelState.AddModelError("", ae.Message);
                }
            }
            else
            {
                var ex = e;
                do
                {
                    ModelState.AddModelError("", ex.Message);

                    ex = ex.InnerException;
                }
                while (ex != null);
            }
        }
        #endregion

        #region Redirect helpers
        public ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }
        #endregion
        
        #region Exists methods
        protected async Task<bool> VodafoneModelExists(string id)
        {
            return await _context.VodafoneModels.AnyAsync(e => e.ModelId == id);
        }

        protected async Task<bool> VodafoneDeviceExists(string id)
        {
            return await _context.VodafeonDevices.AnyAsync(e => e.DeviceId == id);
        }

        protected async Task<bool> VodafoneOTAExists(string modelId, string category, string version)
        {
            return await _context.VodafoneOtas.AnyAsync(o => o.ModelId == modelId && o.Category == category && o.Version == version);
        }

        protected async Task<bool> VodafoneAdminUserExists(string id)
        {
            return await _userManager.Users.AnyAsync(u => u.Id == id);
        }
        #endregion

        #region Dropdownlist
        protected async Task<List<SelectListItem>> GetDropdownListOfModelsWithAllModel(string selectedModelId)
        {
            var models = await _context.VodafoneModels.ToListAsync();
            List<SelectListItem> modelListItem = new List<SelectListItem>();
            modelListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.AllModels,
                Value = String.Empty,
                Selected = String.IsNullOrEmpty(selectedModelId) ? true : false
            });
            foreach (var model in models)
            {
                if (model.ModelId != selectedModelId) {
                    modelListItem.Add(new SelectListItem{
                        Text = model.Name + " ("+ model.ModelId + ")",
                        Value = model.ModelId,
                        Selected = false
                    });
                }
                else
                {
                    modelListItem.Add(new SelectListItem{
                        Text = model.Name + " ("+ model.ModelId + ")",
                        Value = model.ModelId,
                        Selected = true
                    });
                }
            }
            return modelListItem;
        }

        protected async Task<List<SelectListItem>> GetDropdownListOfModelsWithoutAllModel(string selectedModelId)
        {
            var models = await _context.VodafoneModels.ToListAsync();
            List<SelectListItem> modelListItem = new List<SelectListItem>();
            foreach (var model in models)
            {
                if (model.ModelId != selectedModelId) {
                    modelListItem.Add(new SelectListItem{
                        Text = model.Name + " ("+ model.ModelId + ")",
                        Value = model.ModelId,
                        Selected = false
                    });
                }
                else
                {
                    modelListItem.Add(new SelectListItem{
                        Text = model.Name + " ("+ model.ModelId + ")",
                        Value = model.ModelId,
                        Selected = true
                    });
                }
            }
            return modelListItem;
        }

        protected List<SelectListItem> GetDropdownListOfModelOTACategoryWithAllCategory(string selectedCategory)
        {
            List<SelectListItem> categoryListItem = new List<SelectListItem>();
            categoryListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.AllCategories,
                Value = String.Empty,
                Selected = String.IsNullOrEmpty(selectedCategory) ? true : false
            });
            categoryListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.FIRMWARE,
                Value = VF_CONSTANT.FIRMWARE,
                Selected = selectedCategory==VF_CONSTANT.FIRMWARE ? true : false
            });
            categoryListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.MC60,
                Value = VF_CONSTANT.MC60,
                Selected = selectedCategory==VF_CONSTANT.MC60 ? true : false
            });
            return categoryListItem;
        }

        protected List<SelectListItem> GetDropdownListOfModelOTACategory(string selectedCategory)
        {
            List<SelectListItem> categoryListItem = new List<SelectListItem>();
            categoryListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.FIRMWARE,
                Value = VF_CONSTANT.FIRMWARE,
                Selected = selectedCategory==VF_CONSTANT.FIRMWARE ? true : false
            });
            categoryListItem.Add(new SelectListItem{
                Text = VF_CONSTANT.MC60,
                Value = VF_CONSTANT.MC60,
                Selected = selectedCategory==VF_CONSTANT.MC60 ? true : false
            });
            return categoryListItem;
        }

        protected List<SelectListItem> GetDropdownListOfEventType()
        {
            // SOS, 
            // FALL, 
            // REMOVAL, PUT_ON, 
            // LOW_BATTERY, CHARGE_BATTERY,
            // SWITCH_ON, SWITCH_OFF,
            // EMERGENCY_CANCEL
            List<SelectListItem> eventTypeListItem = new List<SelectListItem>();
            eventTypeListItem.Add(new SelectListItem{ Text = "Select Type", Value = string.Empty, Selected = true, Disabled = true });
            eventTypeListItem.Add(new SelectListItem{ Text = "SOS", Value = "SOS", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "FALL", Value = "FALL", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "REMOVAL", Value = "REMOVAL", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "PUT ON", Value = "PUT_ON", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "LOW BATTERY", Value = "LOW_BATTERY", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "CHARGE BATTERY", Value = "CHARGE_BATTERY", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "SWITCH ON", Value = "SWITCH_ON", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "SWITCH OFF", Value = "SWITCH_OFF", Selected = false });
            eventTypeListItem.Add(new SelectListItem{ Text = "EMERGENCY CANCEL", Value = "EMERGENCY_CANCEL", Selected = false });
            return eventTypeListItem;
        }
        #endregion
    }
}