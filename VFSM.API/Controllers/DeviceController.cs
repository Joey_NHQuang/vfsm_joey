using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.DBModels;
using hmx.vodafone.web.Constants;

namespace hmx.vodafone.web.Controllers
{
    // [Authorize(Roles = "Admin, Service Managements")]
    // public class DeviceController : BaseController
    // {
    //     public DeviceController(VodafoneContext context, UserManager<ApplicationUser> userManager)
    //     :base(context, userManager)
    //     {
    //     }

    //     #region Index
    //     public async Task<IActionResult> Index(string currentFilter, string searchString, int? page)
    //     {
    //         if (!string.IsNullOrEmpty(searchString))
    //         {
    //             page = 1;
    //         }
    //         else
    //         {
    //             searchString = currentFilter;
    //         }
    //         ViewData["CurrentFilter"] = searchString;
    //         var deviceList = from m in _context.VodafeonDevices
    //                         select m;
            
    //         if (!String.IsNullOrEmpty(searchString)) 
    //         {
    //             deviceList = deviceList.Where(d => d.DeviceId.Contains(searchString));
    //         }
            
    //         return View(await PaginatedList<VodafoneDevice>.CreateAsync(deviceList.AsNoTracking(), page ?? 1, VF_CONSTANT.PageSize));
    //     }
    //     #endregion

    //     #region Detail
    //     public async Task<IActionResult> Details(string id)
    //     {
    //         if (string.IsNullOrEmpty(id))
    //         {
    //             return NotFound();
    //         }

    //         var vodafoneDevice = await _context.VodafeonDevices
    //             .SingleOrDefaultAsync(m => m.DeviceId == id);
    //         if (vodafoneDevice == null)
    //         {
    //             return NotFound();
    //         }

    //         return View(vodafoneDevice);
    //     }
    //     #endregion

    //     #region Create
    //     [Authorize(Roles = "Admin, Model&Device")]
    //     public async Task<IActionResult> Create()
    //     {
    //         ViewData["ModelList"] = await GetDropdownListOfModelsWithoutAllModel(String.Empty);
    //         return View();
    //     }

    //     [HttpPost]
    //     [ValidateAntiForgeryToken]
    //     [Authorize(Roles = "Admin, Model&Device")]
    //     public async Task<IActionResult> Create([Bind("DeviceId,DeviceVersion,ModelId,Updated,Name,DeviceUid,Imei,IsInit,ImgUrl,Battery,Status,StatusLevel,ConnServerIp,Ip,ChannelId,ActivatedDate,Activated,InactivatedDate,LandLine,Mobile,Address,City")] VodafoneDevice vodafoneDevice)
    //     {
    //         if (ModelState.IsValid)
    //         {
    //             vodafoneDevice.CreatedDate = DateTime.UtcNow;
    //             _context.Add(vodafoneDevice);
    //             await _context.SaveChangesAsync();
    //             return RedirectToAction(nameof(Index));
    //         }
    //         return View(vodafoneDevice);
    //     }
    //     #endregion

    //     #region Edit
    //     [Authorize(Roles = "Admin, Model&Device")]
    //     public async Task<IActionResult> Edit(string id)
    //     {
    //         if (string.IsNullOrEmpty(id))
    //         {
    //             return NotFound();
    //         }

    //         var vodafoneDevice = await _context.VodafeonDevices.SingleOrDefaultAsync(m => m.DeviceId == id);
    //         if (vodafoneDevice == null)
    //         {
    //             return NotFound();
    //         }
    //         return View(vodafoneDevice);
    //     }

    //     [HttpPost]
    //     [ValidateAntiForgeryToken]
    //     [Authorize(Roles = "Admin, Model&Device")]
    //     public async Task<IActionResult> Edit(string id, [Bind("DeviceId,DeviceVersion,ModelId,Updated,Name,DeviceUid,Imei,IsInit,ImgUrl,Battery,Status,StatusLevel,ConnServerIp,Ip,ChannelId,ActivatedDate,Activated,InactivatedDate,LandLine,Mobile,Address,City")] VodafoneDevice vodafoneDevice)
    //     {
    //         if (id != vodafoneDevice.DeviceId)
    //         {
    //             return NotFound();
    //         }

    //         if (ModelState.IsValid)
    //         {
    //             try
    //             {
    //                 vodafoneDevice.ModifiedDate = DateTime.UtcNow;
    //                 _context.Update(vodafoneDevice);
    //                 await _context.SaveChangesAsync();
    //             }
    //             catch (DbUpdateConcurrencyException)
    //             {
    //                 if (!await VodafoneDeviceExists(vodafoneDevice.DeviceId))
    //                 {
    //                     return NotFound();
    //                 }
    //                 else
    //                 {
    //                     throw;
    //                 }
    //             }
    //             return RedirectToAction(nameof(Index));
    //         }
    //         return View(vodafoneDevice);
    //     }
    //     #endregion
    //}
}