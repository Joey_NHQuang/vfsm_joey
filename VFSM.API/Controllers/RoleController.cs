using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using hmx.vodafone.web.Data;
using hmx.vodafone.web.Models;
using hmx.vodafone.web.Models.RoleViewModels;
using hmx.vodafone.web.Models.DBModels;

namespace hmx.vodafone.web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RoleController : BaseController
    {
        private RoleManager<ApplicationRole> _roleManager;

        public RoleController(RoleManager<ApplicationRole> roleManager, VodafoneContext context, UserManager<ApplicationUser> userManager)
        : base(context, userManager)
        {
            _roleManager = roleManager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var roles = await _roleManager.Roles.ToListAsync();
            IList<RoleViewModel> roleViewModels = new List<RoleViewModel>();
            foreach (var role in roles.OrderBy(r => r.Name))
            {
                roleViewModels.Add(new RoleViewModel{ Id = role.Id, RoleName = role.Name });
            }
            
            return View(roleViewModels);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("ID must not be null or empty.");
            }

            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if (role == null)
            {
                return NotFound();
            }

            var userInRole = await _userManager.GetUsersInRoleAsync(role.Name);
            
            return View(new RoleViewModel{ Id = role.Id, RoleName = role.Name, UserCount = userInRole.Count });
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoleViewModel role)
        {
            if(ModelState.IsValid)
            {
                var result = await _roleManager.CreateAsync(new ApplicationRole(role.RoleName));
                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
                this.AddErrors(result);
            }
            return View(role);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if (role == null)
            {
                return NotFound();
            }

            return View(new RoleViewModel{Id = role.Id, RoleName = role.Name});
        }

        // POST: VodafoneModel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if(role.Name == "Admin") 
            {
                throw new ApplicationException($"Unable to delete role with ID '{id}'.");
            }

            var result = await _roleManager.DeleteAsync(role);
            if(result.Succeeded) {
                return RedirectToAction(nameof(Index));
            }
            else 
            {
                throw new ApplicationException(result.Errors.FirstOrDefault().Description);
            }
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var role = await _roleManager.Roles.SingleOrDefaultAsync(r => r.Id == id);
            if (role == null)
            {
                return NotFound();
            }
            return View(new RoleViewModel{Id = role.Id, RoleName = role.Name});
        }

        // POST: VodafoneModel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id, RoleName")] RoleViewModel updateRole)
        {
            if (id != updateRole.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                var role = await _roleManager.FindByIdAsync(updateRole.Id);
                if(role.Name == "Admin") 
                {
                    throw new ApplicationException($"Unable to edit role with ID '{id}'.");
                }

                role.Name = updateRole.RoleName;
                var result = await _roleManager.UpdateAsync(role);
                if (result.Succeeded)
                {
                    return RedirectToAction(nameof(Index));
                }
                this.AddErrors(result);
            }
            return View(updateRole);
        }
    }
}

