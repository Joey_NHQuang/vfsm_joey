﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon;
using hmx.vodafone.web.Config;

namespace VFSM.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailService : IEmailService
    {
        private readonly AWSConfig _awsConfig;

        public EmailService(IOptions<AWSConfig> awsConfig)
        {
            _awsConfig = awsConfig.Value;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            {
                var sendRequest = new SendEmailRequest
                {
                    Source = _awsConfig.AWS_EMAIL_SENDER,
                    Destination = new Destination
                    {
                        ToAddresses =
                        new List<string> { email }
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body
                        {
                            Html = new Content
                            {
                                Charset = "UTF-8",
                                Data = message
                            }
                        }
                    },
                                        
                };
                return client.SendEmailAsync(sendRequest);
            }
        }
    }
}
