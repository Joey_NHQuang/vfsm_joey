﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VFSM.Services
{
    public class ServiceInstaller: Module
    {        
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
           .Where(t => t.Name.Contains("Service") || t.Name.Contains("Helper") || t.Name.Contains("Utility")) 
           .AsImplementedInterfaces();
        }
    }
}
