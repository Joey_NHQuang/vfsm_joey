import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import logo from './logo.svg';

//3rd party
import PerfectScrollbar from 'react-perfect-scrollbar';

//components
import CustomerPage from './components/CustomerPage';
import HistoryPage from './components/HistoryPage';
import SideBar from './common/SideBar/SideBar'
import Header from './common/Header/Header'

//styles

import './styles/App.scss';
import routes from './routes'

class App extends Component {
    render() {

        const flatRoutes = [];
        routes.forEach(group => {
            group.children.forEach(item => {
                flatRoutes.push(item);
            })
        })

        return (
            <div className="app">
                <SideBar />
                <div className="main">
                    <Header />
                    <PerfectScrollbar className="page-content">
                        <Switch>
                            {
                                flatRoutes.map((route, index) => {
                                    return <Route exact path={ `${route.path}` } component={ route.component } key={index}/>;
                                })
                            }
                            {/* <Redirect to={browse} /> */}
                        </Switch>
                    </PerfectScrollbar>
                </div>
                {/* <h1>Hi</h1>
                <img src="images/a.png" className="App-logo" alt="logo" /> */}
            </div>
        );
    }
}

export default App;
