import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import { BrowserRouter } from 'react-router-dom';

import App from './App';
import rootStore from './stores/rootStore'


import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Provider rootStore = {rootStore}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('root')
);



registerServiceWorker();
