import { observable, action, configure, runInAction } from 'mobx';
import { fromJS } from 'immutable';

const dataDefault = fromJS([]);
// don't allow state modifications outside actions
configure({ enforceActions: true });

export default class HomeStore {
    @observable customers = dataDefault;
    constructor(rootStore) {
        this.rootStore = rootStore;
    }

}
