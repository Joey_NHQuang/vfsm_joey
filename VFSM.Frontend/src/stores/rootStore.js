
import customerStore from './customerStore';

export default class RootStore {
    customerStore: customerStore;

    constructor() {
        this.customerStore = new customerStore(this);
    }
}
