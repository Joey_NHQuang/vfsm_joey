import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import DatePicker from '../../common/DatePicker/DatePicker';
import moment from 'moment';
import Select from '../../common/Select/Select';
import { Button } from 'antd';

class CustomersDevices extends Component {
    state = {
        startDate: moment()
    };

    handleChange = (date)=> {
        this.setState({
            startDate: date
        });
    }

    render() {
        return (
            <div>
                <div className="row-end">
                    <DatePicker
                        label = "FROM"
                        className="date-from"
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                    />

                    <DatePicker
                        label = "TO"
                        className="date-to ml-1"
                        selected={this.state.startDate}
                        onChange={this.handleChange}
                    />

                    <Select />
                </div>
                <h2>Customers List</h2>

                <table>
                    <thead>
                        <tr>
                            <th>Created Date</th>
                            <th>Customer ID</th>
                            <th>Name</th>
                            <th>Mobile Type</th>
                            <th>App Version</th>
                            <th>Imei</th>
                            <th>Device</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Aug - 14/2018 09:29:03</td>
                            <td>0b53345-6e93-4008-981</td>
                            <td>unname</td>
                            <td>STAND BY</td>
                            <td>0.0.0</td>
                            <td>8629859981</td>
                            <td>1</td>
                            <td>
                                <Button className="btn-info">Device List</Button>    
                                <Button className="btn-success ml">Register</Button>    
                                <Button className="btn-danger ml">Modify</Button>  
                            </td>
                        </tr>
                        <tr>
                            <td>Aug - 14/2018 09:29:03</td>
                            <td>0b53345-6e93-4008-981</td>
                            <td>unname</td>
                            <td>STAND BY</td>
                            <td>0.0.0</td>
                            <td>8629859981</td>
                            <td>1</td>
                            <td>
                            <Button className="btn-info">Device List</Button>    
                                <Button className="btn-success ml">Register</Button>    
                                <Button className="btn-danger ml">Modify</Button>  
                            </td>
                        </tr>
                        <tr>
                            <td>Aug - 14/2018 09:29:03</td>
                            <td>0b53345-6e93-4008-981</td>
                            <td>unname</td>
                            <td>STAND BY</td>
                            <td>0.0.0</td>
                            <td>8629859981</td>
                            <td>1</td>
                            <td>
                                <Button className="btn-info">Device List</Button>    
                                <Button className="btn-success ml">Register</Button>    
                                <Button className="btn-danger ml">Modify</Button>  
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

};

export default CustomersDevices;
