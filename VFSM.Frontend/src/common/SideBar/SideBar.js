import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { matchPath } from 'react-router'
import { observer } from 'mobx-react';
import routes from '../../routes'
import IconSvg from '../../common/icon/IconSvg'
import './style.scss'



class Sidebar extends Component {

    getFlattenData =()=>{
        const result = [];

        routes.forEach(g=> {
            result.push({
                name: g.groupName,
                icon: g.icon
            });
            g.children.forEach(child =>{
                result.push({
                    name: child.name,
                    path: child.path
                })
            })
        })

        return result;
    }

    isActive = (name)=>{
        const currentPath = this.props.location.pathname;

        for(let g of routes){
            for(let item of g.children){
                if(item.path === currentPath){
                    if(g.groupName == name) {
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
   
    getActiveClassName =(name)=> {
        let className =  this.isActive(name ) ? ' active' : ''
        return className;

    }
    render() {

        const data = this.getFlattenData();

        

        return (
            <div className ="sidebar">
                <div className="sidebar-logo">
                    <img src="images/Logo.png"/>
                </div>
                <div className="sidebar-menu">

                    {
                        data.map((item, index) => {
                            if(item.icon){
                                return <div className="menu-group row" key={index}>
                                            <IconSvg name={item.icon} className={"sidebar-icon" + this.getActiveClassName(item.name)} />
                                            <div className={"menu-group-name" + this.getActiveClassName(item.name)}>{item.name}</div>
                                        </div>
                            }else {
                                return <NavLink
                                            activeClassName='active'
                                            className='menu-item'
                                            to={ item.path }
                                            key={index}>
                                            {item.name}
                                        </NavLink>
                            }
                        })
                    }
                   

                </div>
            </div>

        );
    }

};

export default withRouter(Sidebar);
