import React, { Component } from 'react';
import './style.scss';
import { Select } from 'antd';

const Option = Select.Option;

class SelectBox extends Component {
    state = {
        focused : false
    }

    onFocus = e => {
        this.setState({focused : true})
    }

    onBlur = e => {
        this.setState({focused: false})
    }


    render() {
        const { className, label, ...otherProps } = this.props;
        const { focused } = this.state;
        return (
            
                <Select
                    className ='custom-select'
                    showSearch
                    allowClear
                    style={{ width: 200 }}
                    placeholder="Search type"
                    optionFilterProp="children"
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    placeholder = {<div><span>Type:</span> Search type</div>}
                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="tom">Tom</Option>
                </Select>
        );
    }

};

export default SelectBox;
