import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import './style.scss'
import { Button } from 'antd';

class Header extends Component {
    render() {
        return (
            <div className="header row">
                <div className="inner-left column flex">
                    <div className="row-between full-height">
                        <h1 className="title full-height row ml-6">Admin Users</h1>
                        <div className="row right mr-3">
                            <Button className="btn-secondary">Back To Full List</Button>
                            <Button className="btn-primary ml-3">Create New</Button>
                        </div>
                    </div>
                </div>
                <div className="user-info column">
                    <h1>Danny</h1>
                </div>
            </div>
        );
    }

};

export default Header;
