import React, { Component } from 'react';
import ReactSVG from 'react-svg'

class IconSvg extends Component {
    render() {
        const { name, ...otherProps } = this.props;
        return (
            <ReactSVG path={'images/' + this.props.name + '.svg'}  {...otherProps}/>

        );
    }

};

export default IconSvg;
