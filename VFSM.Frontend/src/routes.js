import CustomersDevices from './components/CustomersDevices/CustomersDevices';
import IntegrationHistory from './components/IntegrationHistory/IntegrationHistory';
import BeaconLocation from './components/BeaconLocation/BeaconLocation';
import Report from './components/Report/Report';
import Models from './components/Models/Models';
import OTA from './components/OTA/OTA';

const routes = [
    {
        groupName:'CS Operation',
        icon:'CS OPERATION@2x',
        children: [
            {
                name:'Customers & Devices',
                path: '/customers-and-devices',
                component: CustomersDevices,
            }, 
            {
                name:'Integration History',
                path: '/integration-history',
                component: IntegrationHistory
            }, 
            {
                name:'Beacon Location',
                path: '/beacon-location',
                component: BeaconLocation
            }, 
            {
                name:'Report',
                path: '/report',
                component: Report,
            }
        ]
    }, 
    {
        groupName: 'Service Managements',
        icon:'Service Managements@2x',
        children: [
            {
                name:'Models',
                path:'/models',
                component: Models
            },
            {
                name:'OTA',
                path:'/OTA',
                component: OTA
            }
        ]
    }
];

export default routes;
