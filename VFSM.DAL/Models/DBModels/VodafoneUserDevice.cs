using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hmx.vodafone.web.Models.DBModels
{
    [Table("T_USER_DEVICE")]
    public class VodafoneUserDevice
    {
        [Key]
        [Required]
        [Column("USER_ID", Order = 0)]
        [StringLength(36)]
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Key]
        [Required]
        [Column("DEVICE_ID", Order = 1)]
        [StringLength(36)]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }

        [Required]
        [Column("REG_NAME")]
        [StringLength(128)]
        [Display(Name = "Reg Name")]
        public string RegName { get; set; }

        [Required]
        [Column("ROLE")]
        [Display(Name = "Role")]
        public string Role { get; set; }

        [Column("SHARE_ID")]
        [StringLength(36)]
        [Display(Name = "Share Id")]
        public string ShareId { get; set; }

        [Column("MANAGER_USER_ID")]
        [StringLength(36)]
        [Display(Name = "Manager User Id")]
        public string ManagerUserId { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Create Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
    }
}