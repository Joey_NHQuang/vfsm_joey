using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hmx.vodafone.web.Models.DBModels
{
    [Table("T_ACTIVITY")]
    public class VodafoneActivity
    {
        [Key]
        [Required]
        [Column("ACT_ID")]
        [StringLength(36)]
        [Display(Name = "Activity Id")]
        public string ActivityId { get; set; }

        [Required]
        [Column("DEVICE_ID")]
        [StringLength(36)]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }

        [Required]
        [Column("EVENT_TYPE")]
        [Display(Name = "Event Type")]
        public string EventType { get; set; }

        [Required]
        [Column("STATUS_LEVEL")]
        [Display(Name = "Status Level")]
        public string StatusLevel { get; set; }

        [Required]
        [Column("LOC_SOURCE")]
        [Display(Name = "Log Source")]
        public string LogSource { get; set; }

        [Column("LAT")]
        [Display(Name = "LAT")]
        public double Lat { get; set; }

        [Column("LNG")]
        [Display(Name = "LNG")]
        public double Lng { get; set; }

        [Column("BEACON_MAC")]
        [StringLength(24)]
        [Display(Name = "Beacon Mac")]
        public string BeaconMac { get; set; }

        [Required]
        [Column("IS_SERIES", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Is Series")]
        public bool  IsSeries { get; set; }

        [Column("BATTERY")]
        [Display(Name = "Battery")]
        public int Battery { get; set; }

        [Column("TAKER_ID")]
        [StringLength(36)]
        [Display(Name = "Taker Id")]
        public string TakerId { get; set; }

        [Required]
        [Column("IS_SHARE_TAKEN_USER_LOCATION", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Is Share Taken User Location")]
        public bool  IsShareTakenUserLocation { get; set; }

        [Required]
        [Column("RESOLVED", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Resolved")]
        public bool  Resolved { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
    }
}