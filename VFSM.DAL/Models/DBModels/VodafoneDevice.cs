using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace hmx.vodafone.web.Models.DBModels
{
    [Table("T_DEVICE")]
    public class VodafoneDevice 
    {
        [Key]
        [Required]
        [Column("DEVICE_ID")]
        [StringLength(36)]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }

        [Required]
        [Column("DEVICE_VERSION")]
        [StringLength(20)]
        [Display(Name = "Version")]
        public string DeviceVersion { get; set; }

        [Column("MODEL_ID")]
        [StringLength(20)]
        [Display(Name = "Model Id")]
        public string ModelId { get; set; }

        [Required]
        [Column("CHIPSET_VERSION")]
        [StringLength(20)]
        [Display(Name = "Chipset Version")]
        public string ChipsetVersion { get; set; }

        [Column("NAME")]
        [StringLength(128)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Column("DEVICE_UID")]
        [StringLength(128)]
        [Display(Name = "Device UID")]
        public string DeviceUid { get; set; }

        [Required]
        [Column("IS_INIT", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Is Init")]
        public bool  IsInit { get; set; }

        [Column("IMG_URL")]
        [StringLength(255)]
        [Display(Name = "Image URL")]
        public string ImgUrl { get; set; }

        [Column("BATTERY")]
        [Display(Name = "Battery")]
        public int? Battery { get; set; }

        [Required]
        [Column("STATUS")]
        [DefaultValue("[\"OK\"]")]
        [StringLength(64)]
        [Display(Name = "Status")]
        public string Status { get; set; }

        [Required]
        [Column("STATUS_LEVEL")]
        [Display(Name = "Status Level")]
        public string StatusLevel { get; set; }

        [Column("CONN_SERVER_IP")]
        [StringLength(32)]
        [Display(Name = "Server IP")]
        public string ConnServerIp { get; set; }

        [Column("IP")]
        [StringLength(24)]
        [Display(Name = "IP")]
        public string Ip { get; set; }

        [Column("CHANNEL_ID")]
        [StringLength(60)]
        [Display(Name = "Channel ID")]
        public string ChannelId { get; set; }

        [Column("ACTIVATED_DATE")]
        [Display(Name = "Activated Date")]
        public DateTime? ActivatedDate { get; set; }

        [Required]
        [Column("ACTIVATED", TypeName = "bit")]
        [DefaultValue(false)]
        [Display(Name = "Is Activated")]
        public bool Activated { get; set; }

        [Column("INACTIVATED_DATE")]
        [Display(Name = "Inactivated Date")]
        public DateTime? InactivatedDate { get; set; }

        [Column("LANDLINE")]
        [StringLength(32)]
        [Display(Name = "Land Line")]
        public string LandLine { get; set; }

        [Column("MOBILE")]
        [StringLength(32)]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [Column("ADDRESS")]
        [StringLength(1024)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Column("CITY")]
        [StringLength(64)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime ModifiedDate { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Create Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime CreatedDate { get; set; }
    }
}