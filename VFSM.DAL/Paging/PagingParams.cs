﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VinaHR.DAL
{
    public class PagingParams
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int TotalItems { get; set; }
    }
}
